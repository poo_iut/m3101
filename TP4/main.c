#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

typedef void * ( * PtrFct) (void *);

void * thread1(void * val){
    printf("\tEXEC THREAD 1\n");
    // Transtypage du paramètre val (void *) vers num (int)
    int num = *((int*)val);
    num*=2;
    // Recherche du THREAD ID
    pthread_t tid;
    tid = pthread_self();
    printf("\t TID thread1: %ld \n", tid);
    return (void*) num;
}

void * thread2(void * val){
    printf("\tEXEC THREAD 2\n");
    // Transtypage du paramètre val (void *) vers num (int)


    pthread_t tid;
    tid = pthread_self();
    printf("\t TID thread2: %ld \n", tid);
    char* texte = (char*) val;
    // Affichage du message
    printf("\t MESSAGE : %s\n", texte);

    // Allocation zone mémoire pour pouvoir utiliser
    // dans le programme principal
    // Sinon ça ne marche pas
    char* buffer = malloc(sizeof(char) * 1024);
    // Concaténation
    strcpy(buffer, texte);
    strcat(buffer, " Message de Thread2");

    printf("\t BUFFER: %s\n", buffer);

    return buffer;
}

int main(int argc, char const *argv[]) {
    printf("TEST\n");


    pthread_attr_t attrThread; // Attribut du Thread
    // Création des attributs du Thread (Attribut par défaut)
    int err = pthread_attr_init(&attrThread); // Erreur
    if(err){
        perror("Erreur init attributs Thread1");
        exit(1);
    }

    //int pthread_create ( pthread_t * Id, pthread_attr_t * attr, PtrFct traite, void * param ) ;

    pthread_t threadN1; // Thread n1
    // Création du thread avec la fonction thread1()
    int a = 3;
    err = pthread_create(&threadN1, &attrThread, thread1, (void *) &a);
    if(err){
        perror("Erreur création Thread1");
        exit(1);
    }

    pthread_t threadN2;

    char* texte = "Goungi";
    err = pthread_create(&threadN2, &attrThread, thread2, (void *) texte);

    // Join du thread 1
    int retour;
    pthread_join(threadN1, (void*) &retour);
    printf("Retour du Thread1 : %d\n", retour);

    void* retourMessage;
    // Join du thread 2, + stockage du retour de la fonction dans un char* cast en tant que que void*
    pthread_join(threadN2, &retourMessage);

    char* retourTexte = (char*) retourMessage;

    printf("Retour du Thread 2: %s\n", retourTexte);
    printf("FIN PROGRAMME MAIN\n");

    // On libère l'espace mémoire alloué par le malloc()
    // dans thread2()
    free(retourTexte);

    return 0;
}
