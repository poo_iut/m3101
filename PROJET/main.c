/**
*
*
* Fabien CAYRE
* Dorian GARDES
*
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/socket.h>
#include  <netdb.h>
#include  <stdbool.h>
#include  <string.h>
#include <unistd.h>

#define SERVADDR "localhost"        // Définition de l'adresse IP d'écoute
#define SERVPORT "0"                // Définition du port d'écoute,
                                    // si 0 port choisi dynamiquement
#define LISTENLEN 1                 // Taille du tampon de demande de connexion
#define MAXBUFFERLEN 1024
#define MAXHOSTLEN 64
#define MAXPORTLEN 6

void cleanRead(int socket, char* buffer){
	int ecode;
	ecode = read(socket, buffer, MAXBUFFERLEN);
	if(ecode == -1){
		perror("Erreur lecture");
		exit(2);
	}
	buffer[ecode] = '\0';
}

bool strstart(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? false : memcmp(pre, str, lenpre) == 0;
}

void cleanWrite(int socket, char* buffer){
	write(socket, buffer, strlen(buffer));
}

int openConnection(const char* address, const char* port){
	int tempEcode;                     // Retour des fonctions
	struct addrinfo *tempRes,*tempResPtr;  // Résultat de la fonction getaddrinfo
	struct addrinfo tempHints;
	int retSocket;
	// booléen indiquant que l'on est bien connecté
	bool isConnected = false;
    // Initailisation de serverHints
	memset(&tempHints, 0, sizeof(tempHints));
	tempHints.ai_socktype = SOCK_STREAM;  // TCP
	tempHints.ai_family = AF_INET;      // les adresses IPv4 et IPv6 seront présentées par
				                          // la fonction getaddrinfo

	//Récupération des informations sur le serveur
	//serverEcode = getaddrinfo(serverName,serverPort,&serverHints,&serverRes);
	tempEcode = getaddrinfo(address,port,&tempHints,&tempRes);
	if (tempEcode){
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(tempEcode));
		exit(1);
	}

	tempResPtr = tempRes;
	isConnected = false;
	while(!isConnected && tempResPtr!=NULL){

		//Création de la socket IPv4/TCP
		retSocket = socket(tempResPtr->ai_family, tempResPtr->ai_socktype, tempResPtr->ai_protocol);
		//perror("LIGNE 153: LE SOCKET SERVEUR RENVOIE UNE VALEUR ABERRANTE ! puisqu'il écrit sur la console");
		printf("SocketID : %d\n", retSocket);
		if (retSocket == -1) {
			perror("Erreur creation socket");
			exit(2);
		}

  		//Connexion au serveur
		printf("Trying connecting to... (%s:%s)\n", address, port);
		tempEcode = connect(retSocket, tempResPtr->ai_addr, tempResPtr->ai_addrlen);
		if (tempEcode == -1) {
			tempResPtr = tempResPtr->ai_next;
			printf("Trying...\n");
			close(retSocket);
		}
		// On a pu se connecter
		else isConnected = true;
	}
	freeaddrinfo(tempRes);
	if (!isConnected){
		perror("Connexion impossible");
		exit(2);
	}

	return retSocket;
}

int main(){
	int ecode;                       // Code retour des fonctions
	char proxyAddr[MAXHOSTLEN];     // Adresse du serveur
	char proxyPort[MAXPORTLEN];     // Port du server
	int pendingSocket;                 // Descripteur de socket de rendez-vous
	int clientSocket;                 // Descripteur de socket de communication
	struct addrinfo proxyHints;           // Contrôle la fonction getaddrinfo
	struct addrinfo *res;            // Contient le résultat de la fonction getaddrinfo
	struct sockaddr_storage myinfo;  // Informations sur la connexion de RDV
	struct sockaddr_storage from;    // Informations sur le client connecté
	socklen_t len;                   // Variable utilisée pour stocker les
				                        // longueurs des structures de socket
	char buffer[MAXBUFFERLEN];       // Tampon de communication entre le client et le serveur

	// Publication de la socket au niveau du système
	// Assignation d'une adresse IP et un numéro de port
	// Mise à zéro de proxyHints
	memset(&proxyHints, 0, sizeof(proxyHints));
	// Initailisation de proxyHints
	proxyHints.ai_flags = AI_PASSIVE;      // mode serveur, nous allons utiliser la fonction bind
	proxyHints.ai_socktype = SOCK_STREAM;  // TCP
	proxyHints.ai_family = AF_INET;      // les adresses IPv4 et IPv6 seront présentées par
				                         // la fonction getaddrinfo

	// Récupération des informations du serveur
	ecode = getaddrinfo(SERVADDR, SERVPORT, &proxyHints, &res);
	if (ecode) {
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(ecode));
		exit(1);
	}

	//Création de la socket IPv4/TCP
	pendingSocket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (pendingSocket == -1) {
		perror("Erreur creation socket");
		exit(4);
	}

	// Publication de la socket
	ecode = bind(pendingSocket, res->ai_addr, res->ai_addrlen);
	if (ecode == -1) {
		perror("Erreur liaison de la socket de RDV");
		exit(3);
	}
	// Nous n'avons plus besoin de cette liste chainée addrinfo
	freeaddrinfo(res);
	// Récuppération du nom de la machine et du numéro de port pour affichage à l'écran

	len=sizeof(struct sockaddr_storage);
	ecode=getsockname(pendingSocket, (struct sockaddr *) &myinfo, &len);
	if (ecode == -1)
	{
		perror("SERVEUR: getsockname");
		exit(4);
	}
	ecode = getnameinfo((struct sockaddr*)&myinfo, sizeof(myinfo), proxyAddr,MAXHOSTLEN,
                         proxyPort, MAXPORTLEN, NI_NUMERICHOST | NI_NUMERICSERV);
	if (ecode != 0) {
		fprintf(stderr, "error in getnameinfo: %s\n", gai_strerror(ecode));
		exit(4);
	}
	printf("L'adresse d'ecoute est: %s\n", proxyAddr);
	printf("Le port d'ecoute est: %s\n", proxyPort);
	// Definition de la taille du tampon contenant les demandes de connexion
	ecode = listen(pendingSocket, LISTENLEN);
	if (ecode == -1) {
		perror("Erreur initialisation buffer d'écoute");
		exit(5);
	}

	// Serveur prêt ! Attente d'un client.

	len = sizeof(struct sockaddr_storage);
	// Attente connexion du client
	// Lorsque demande de connexion, creation d'une socket de communication avec le client
	clientSocket = accept(pendingSocket, (struct sockaddr *) &from, &len);
	if (clientSocket == -1){
		perror("Erreur accept\n");
		exit(6);
	}else{
		printf("Client connecté.\n");
	}

	strcpy(buffer, "220 localhost proxy OK\n");

	write(clientSocket, buffer, strlen(buffer));

	// Echange de données avec le client connecté
	ecode = read(clientSocket, buffer, MAXBUFFERLEN);
	if (ecode == -1) {
		// S'il y a une erreur dans la lecture
		perror("Problème de lecture\n");
		exit(3);
	}
	buffer[ecode] = '\0';
	printf("Full Adress : %s", buffer);

	char user[MAXBUFFERLEN];
	char serverName[MAXHOSTLEN];
	char serverPort[5] = "";
	sscanf(buffer, "%[^@]@%[^:]:%s", user, serverName, serverPort);

	if(strcmp(serverPort, "") == 0){
		strcpy(serverPort, "21");
		serverName[strlen(serverName)-2] = '\0';
	}// else serverName[strlen(serverName)] = '\0';

	printf("Try login to \"%s\" on port \"%s\" with \"%s\"\n", serverName, serverPort, user);

	/* ICI  = SERVEUR */
	int serverSocket;                  // Descripteur de la socket
	int serverEcode;                     // Retour des fonctions
	struct addrinfo *serverRes,*serverResPtr;  // Résultat de la fonction getaddrinfo
	struct addrinfo serverHints;

	bool isConnected = false;      // booléen indiquant que l'on est bien connecté


    // Initailisation de serverHints
	memset(&serverHints, 0, sizeof(serverHints));
	serverHints.ai_socktype = SOCK_STREAM;  // TCP
	serverHints.ai_family = AF_INET;      // les adresses IPv4 et IPv6 seront présentées par
				                          // la fonction getaddrinfo

	//Récupération des informations sur le serveur
	//serverEcode = getaddrinfo(serverName,serverPort,&serverHints,&serverRes);
	serverEcode = getaddrinfo(serverName,serverPort,&serverHints,&serverRes);
	if (serverEcode){
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(serverEcode));
		exit(1);
	}

	serverResPtr = serverRes;

	while(!isConnected && serverResPtr!=NULL){

		//Création de la socket IPv4/TCP
		serverSocket = socket(serverResPtr->ai_family, serverResPtr->ai_socktype, serverResPtr->ai_protocol);
		//perror("LIGNE 153: LE SOCKET SERVEUR RENVOIE UNE VALEUR ABERRANTE ! puisqu'il écrit sur la console");
		printf("SocketID : %d\n", serverSocket);
		if (serverSocket == -1) {
			perror("Erreur creation socket");
			exit(2);
		}

  		//Connexion au serveur
		printf("Trying connecting to server... \n");
		ecode = connect(serverSocket, serverResPtr->ai_addr, serverResPtr->ai_addrlen);
		if (ecode == -1) {
			serverResPtr = serverResPtr->ai_next;
			printf("Trying...\n");
			close(serverSocket);
		}
		// On a pu se connecter
		else isConnected = true;
	}
	freeaddrinfo(serverRes);
	if (!isConnected){
		perror("Connexion impossible");
		exit(2);
	}
	printf("Connected!\n");
	printf("Reading...\n");
	// On reçoit le "220" venant du serveur
	ecode = read(serverSocket, buffer, MAXBUFFERLEN);
	if(ecode == -1){
		perror("Erreur de lecture pour lire aprés envoyé le USER");
		exit(2);
	}
	buffer[ecode] = '\0';
	printf("Received : %s", buffer);


	user[strlen(user)+1] = '\0';
	user[strlen(user)] = '\n'; // On ajoute un \n à la fin du buffer


	// On écrit dans le buffer : "USER <user>"
	strcpy(buffer, user);

	// On envoie le USER
	printf("Envoie du message USER au serveur\n");
	printf("Sending : %s", buffer);
	write(serverSocket, buffer, strlen(buffer));
	printf("Message envoyé.\n");


	// On renvoie le message reçu par le serveur au client

	// Reception de la demande de passwd
	ecode = read(serverSocket, buffer, MAXBUFFERLEN);
	if(ecode == -1){
		perror("Erreur lecture du passwd");
		exit(2);
	}
	buffer[ecode] = '\0';
	printf("Received : \"%s\"", buffer);
	// On envoie le passwd au client
	write(clientSocket, buffer, strlen(buffer));

	printf("Waiting for password...\n");
	// Lecture du password
	ecode = read(clientSocket, buffer, MAXBUFFERLEN);
	if(ecode == -1){
		perror("Erreur lecture");
		exit(2);
	}
	buffer[ecode] = '\0';
	printf("Receive password from client!\n");
	printf("Writing to server...\n");
	// on écrit le password
	write(serverSocket, buffer, strlen(buffer));

	// Lecture du serveur
	ecode = read(serverSocket, buffer, MAXBUFFERLEN);
	if(ecode == -1){
		perror("Erreur lecture");
		exit(2);
	}
	buffer[ecode] = '\0';
	printf("Received : \"%s\"", buffer);
	printf("Sending 230 to client...");
	write(clientSocket, buffer, strlen(buffer));

	// Lecture du client
	ecode = read(clientSocket, buffer, MAXBUFFERLEN);
	if(ecode == -1){
		perror("Erreur lecture");
		exit(2);
	}
	buffer[ecode] = '\0';
	printf("Received : %s", buffer);
	printf("Sending SYST to server...");
	write(serverSocket, buffer, strlen(buffer));

	// Recup 215
	ecode = read(serverSocket, buffer, MAXBUFFERLEN);
	if(ecode == -1){
		perror("Erreur lecture");
		exit(2);
	}
	buffer[ecode] = '\0';
	printf("Received : %s", buffer);
	printf("Sending 215 to client...\n");
	write(clientSocket, buffer, strlen(buffer));
	printf("215 send\n");


	// Attente message du CLIENT
	// LE CLIENT ENVOI QUIT POUR SE DECO
	char* quitMsg = "QUIT\n";

	// Writing to socket
	cleanWrite(clientSocket, buffer);
	int i = 0;
	while(1){
		cleanRead(clientSocket, buffer);
		if(strcmp(buffer, quitMsg) == 0){
			printf("QUIT received from client");
			break;
		}
		printf("\n");
		printf("Received from client: %s", buffer);
		i++;
		printf("Number of receive : %d\n", i);
		printf("\n");
		char portData[512];
		strcpy(portData, buffer);
		// LE client envoie PORT

		printf("\n");
		// Extraire le port client
		char dataConnectionC[512];
		int c1, c2, c3, c4, nc1, nc2;
		strcpy(dataConnectionC, buffer);
		sscanf(dataConnectionC, "PORT %d,%d,%d,%d,%d,%d", &c1,&c2,&c3,&c4,&nc1,&nc2);
		int IclientPassivePort = nc1*256+nc2;
		char clientPassiveAddress[MAXBUFFERLEN];
		char clientPassivePort[MAXPORTLEN];
		sprintf(clientPassiveAddress, "%d.%d.%d.%d", c1,c2,c3,c4);
		sprintf(clientPassivePort, "%d", IclientPassivePort);
		printf("Client Passive - %s:%s\n", clientPassiveAddress, clientPassivePort);
		printf("\n");


		printf("\n");

		// Envoie passive mode au serveur
		// Fait rentrer le proxy en mode passif
		strcpy(buffer, "PASV\n");
		printf("Sending  to server: %s", buffer);

		// Envoie du message au serveur
		cleanWrite(serverSocket, buffer);
		// Réponse du serveur
		cleanRead(serverSocket, buffer);

		// Message du serveur -> Couple IP : port
		char dataConnectionS[512];
		int b1, b2, b3, b4, n1, n2;
		strcpy(dataConnectionS, buffer);
	    sscanf(dataConnectionS,"227 Entering Passive Mode (%d,%d,%d,%d,%d,%d)",
					&b1,&b2,&b3,&b4,&n1,&n2);

		// Calcul du port
	    int IserverPassivePort = n1*256 + n2;
		printf("\n");
		char serverPassiveAddress[MAXBUFFERLEN];
		char serverPassivePort[MAXBUFFERLEN];
		// b1,b2,b3,b4 -> serverAdressPassv
		sprintf(serverPassiveAddress, "%d.%d.%d.%d", b1, b2, b3, b4);
		sprintf(serverPassivePort, "%d", IserverPassivePort);
		printf("Server Passive - %s:%s\n", serverPassiveAddress, serverPassivePort);

		// Ouverture de la connexion client
		int dataServer = openConnection(clientPassiveAddress, clientPassivePort);

		int clientServer = openConnection(serverPassiveAddress, serverPassivePort);
	}

	printf("Sending quit to server...");
	strcpy(buffer, quitMsg);
	write(serverSocket, buffer, strlen(buffer));


	// crifo.org
	printf("Déconnexion\n");
	//Fermeture de la connexion
	close(clientSocket);
	close(pendingSocket);
}
