/***********************************************
*  ASR => M3101                                *
************************************************
*  Login :                                     *
*                                              *
************************************************
*  Groupe:                                     *
*                                              *
************************************************
*  Nom-prénom :                                *
*  Nom-prénom :                                *
*                                              *
*                                              *
************************************************
*  TP n°:                                      *
************************************************
*  Nom du fichier :                            *
***********************************************/

#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

void waitProcessToEnd(){
    int rapport, numSig, status;
    pid_t retour = wait(&rapport);
    while(retour != -1){
        numSig = rapport & 0x7F;
        switch (numSig) {
            case 0: /* fin normale */
    			status = (rapport >> 8) & 0xFF;
    			printf("Fin normale du fils PID = %d, status = %d\n", retour, status);
                break;
    		default: /* fin anormale */
    			printf("Fin anormale du fils (numSig = %d, pid = %d)\n", numSig, retour);
                break;
        }
        retour = wait(&rapport);
    }
    printf("Fin de l'attente du processus père.\n");
}

int main(int argc, char const *argv[]) {

    time_t t = time(NULL);

    uid_t uid = getuid();
    char* login = getlogin();

    printf("Date: %s, Login: %s, UID: %d\n", ctime(&time), login, uid);

    /* Processus 1:*/
    pid_t pFils1 = fork();
    switch(pFils1){
        case -1:
            printf("Erreur de création du processus pFils1\n");
            exit(1);
        case 0:
            printf("Création avec succés (pFils1)\n");
            printf("Fin du processus (pFils1)\n");
            printf("PID pFils1: %d (pFils1)\n", getpid());
            exit(2);
    }

    /* Processus 2:*/
    pid_t pFils2 = fork();
    switch(pFils2){
        case -1:
            printf("Erreur de création du processus pFils2\n");
            exit(0);
        case 0:
            printf("Création avec succés (pFils2)\n");
            printf("Fin du processus (pFils2)\n");
            printf("PID pFils2: %d (pFils2)\n", getpid());
            int* nullPnt = NULL;
            *nullPnt = 54;
            exit(1);
    }

    /* Processus 3 */
    pid_t pFils3 = fork();
    switch(pFils3){
        case -1:
            printf("Erreur de création du processus pFils3\n");
            exit(0);
        case 0:
            printf("Création avec succés (pFils3)\n");
            printf("Fin du processus (pFils3)\n");
            printf("PID pFils3: %d (pFils3)\n", getpid());
            int a = 0;
            int b = 5 / a;
            exit(1);
    }

    waitProcessToEnd();

    return 0;
}
