
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

void waitProcessToEnd(){
    int rapport, numSig, status;
    pid_t retour = wait(&rapport);
    while(retour != -1){
        numSig = rapport & 0x7F;
        switch (numSig) {
            case 0: /* fin normale */
    			status = (rapport >> 8) & 0xFF;
    			printf("Fin normale du fils PID = %d, status = %d\n", retour, status);
                break;
    		default: /* fin anormale */
    			printf("Fin anormale du fils (numSig = %d, pid = %d)\n", numSig, retour);
                break;
        }
        retour = wait(&rapport);
    }
    printf("Fin de l'attente du processus père.\n");
}

int main(int argc, char const *argv[]) {
    /* code */

    char* argFils2[] = {"/home/fabien/atom/c/M3101/ex2/p2fils2", "123456", NULL};

    printf("Appel des sous-programme...\n");

    pid_t pFils1 = fork();
    switch(pFils1){
        case -1:
            printf("Erreur de création du processus pFils1\n");
            exit(1);
        case 0:
            printf("Création avec succés (pFils1)\n");
            execl("p2fils1", "p2fils1", "Test argument1", "Test argument2", NULL);
            exit(1);
    }

    printf("PID Fils 1 = %d\n", pFils1);

    pid_t pFils2 = fork();
    switch(pFils2){
        case -1:
            printf("Erreur de création du processus pFils2\n");
            exit(1);
        case 0:
            printf("Création avec succés (pFils2)\n");
            execvp(argFils2[0], argFils2);
            printf("Echec (pFils2)\n");
            exit(1);
    }


    printf("PID Fils 2 = %d\n", pFils2);

    printf("Appel finis.\n");
    waitProcessToEnd();
    return 0;
}
