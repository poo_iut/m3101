
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

int main(int argc, char const *argv[]) {
    printf("\tAppel du sous-programme 2\n");
    if(argc != 2){
        printf("\tVeuillez renseigner un argument.\n");
        exit(1);
    }
    int i = 0;
    i = atoi(argv[1]);
    printf("\tArgument décimal : %d, Argument en Hexadécimal: 0x%x\n", i, i);
    exit(1);
}
