/***********************************************
*  ASR => M3101                                *
************************************************
*  Login :                                     *
*                                              *
************************************************
*  Groupe:                                     *
*                                              *
************************************************
*  Nom-prénom :                                *
*  Nom-prénom :                                *
*                                              *
*                                              *
************************************************
*  TP n°:                                      *
************************************************
*  Nom du fichier :                            *
***********************************************/
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>

//#define DEBUG

void waitProcessToEnd(int fichier[]){
    int rapport, numSig, status;
    pid_t retour = wait(&rapport);
    while(retour != -1){
        numSig = rapport & 0x7F;
        switch (numSig) {
            case 0: /* fin normale */
    			status = (rapport >> 8) & 0xFF;

    			printf("Fin normale du fils PID = %d, status = %d\n", retour, status);
                break;
    		default: /* fin anormale */
    			printf("Fin anormale du fils (numSig = %d, pid = %d)\n", numSig, retour);
                break;
        }
        retour = wait(&rapport);
    }
    printf("Fin de l'attente du processus père.\n");
}

void fils(int fichier[]){
    char buffer[1024];
    read(fichier[0], buffer, 1024);
    printf("Valeur du pipe : %s\n", buffer);
    close(fichier[0]);
}

int main(int argc, char const *argv[]) {
    FILE *tete, *queue;
    int fichier[2];
    int err;
    err = pipe(fichier);
    if(err == -1){
        printf("Echec pipe\n");
        exit(-1);
    }
    tete = fdopen(fichier[0], "r");
    queue = fdopen(fichier[1], "w");

    printf("Id Tête %d, Id Queue %d\n", fichier[0], fichier[1]);

    //Ecriture du pipe

    char* line = "";
    size_t taille = 0;
    char* stopCmd = "stop";
    char buff[1024];

    printf("Veuillez écrire des phrases, et saisir %s pour arreter\n", stopCmd);

    while(strcmp(line, stopCmd) != 10){
        getline(&line, &taille, stdin);
        if(strcmp(stopCmd, line) == 10){
            break;
        }
        #ifdef DEBUG
            printf("Retour du strcmp %d\n", strcmp(line, stopCmd));
            printf("Taille de la ligne: %ld, Taille de la commande: %ld\n", sizeof(line), sizeof(stopCmd));
        #endif
        strcpy(buff, line);
        write(fichier[1], buff, taille);
        close(fichier[1]);
        printf("Ligne saisie et écrite: %s\n", line);
        printf("Veuillez saisir une ligne... (commande pour arreter : %s)\n", stopCmd);
    }

    pid_t fils_pid = fork();
    switch(fils_pid){
        case -1:
            printf("Echec création du fils 1\n");
            exit(0);
        case 0:
            printf("Création du fils\n");
            fils(fichier);
            exit(0);
    }
    waitProcessToEnd(fichier);
    return 0;
}
