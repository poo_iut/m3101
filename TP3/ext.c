#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>

int main(int argc, char const *argv[]) {
    FILE *tete, *queue;
    int fichier[2];
    int err;
    err = pipe(fichier);
    if(err == -1){
        printf("Echec pipe\n");
        exit(-1);
    }
    tete = fdopen(fichier[0], "r");
    queue = fdopen(fichier[1], "w");

    printf("Id Tête %d, Id Queue %d\n", fichier[0], fichier[1]);

    //Ecriture du pipe

    char* line = "";
    size_t taille = 0;
    char* stopCmd = "stop";
    char buff[1024];

    printf("Veuillez écrire des phrases, et saisir %s pour arreter\n", stopCmd);

    while(strcmp(line, stopCmd) != 10){
        getline(&line, &taille, stdin);
        #ifdef DEBUG
            printf("Retour du strcmp %d\n", strcmp(line, stopCmd));
            printf("Taille de la ligne: %ld, Taille de la commande: %ld\n", sizeof(line), sizeof(stopCmd));
        #endif
        strcpy(buff, line);
        fprintf(queue, line);
        fflush(queue);
        printf("Ligne saisie et écrite: %s\n", line);
        printf("Veuillez saisir une ligne... (commande pour arreter : %s)\n", stopCmd);
    }

    pid_t fils = fork();
    switch(fils){
        case -1:
            printf("Echec création du fils 1\n");
            exit(0);
        case 0:
            printf("Création du fils\n");
            while(line != NULL){
                getline(&line, &taille, tete);
                printf("Ligne: %s", line);
            }
            close(fichier[0]);
            exit(0);
    }
}
