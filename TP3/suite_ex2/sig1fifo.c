
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>

#define FIFO_NAME "goungi_fifo"

int main(int argc, char const *argv[]) {
    int idFifo = open(FIFO_NAME, O_RDONLY);
    if(idFifo == -1){
        printf("Erreur ouverture fifo\n");
        exit(1);
    }
    char str[80];
    read(idFifo, str, 80);
    int pid = atoi(str);
    printf("\tMon PID issou : %d\n", pid);

    return 0;
}
