#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <string.h>
#include <fcntl.h>

#define FIFO_NAME "goungi_fifo"

void waitProcessToEnd(int fichier[]){
    int rapport, numSig, status;
    pid_t retour = wait(&rapport);
    while(retour != -1){
        numSig = rapport & 0x7F;
        switch (numSig) {
            case 0: /* fin normale */
    			status = (rapport >> 8) & 0xFF;

    			printf("Fin normale du fils PID = %d, status = %d\n", retour, status);
                break;
    		default: /* fin anormale */
    			printf("Fin anormale du fils (numSig = %d, pid = %d)\n", numSig, retour);
                break;
        }
        retour = wait(&rapport);
    }
    printf("Fin de l'attente du processus père.\n");
}

int main(int argc, char const *argv[]) {
    int err;
    err = mkfifo(FIFO_NAME, 0600);
    if(err == -1){
        printf("Erreur création fifo\n");
        exit(1);
    }
    printf("Création du fifo effectuée\n", err);
    pid_t sig1fifo = fork();
    switch(sig1fifo){
        case -1:
            printf("Erreur création fils1\n");
            exit(1);
            break;
        case 0:
            printf("\tCréation du fils");
            execl("sig1fifo", "sig1fifo", NULL);
            break;
    }
    int idFifo = open(FIFO_NAME, O_WRONLY);
    if(idFifo == -1){
        printf("Erreur ouverture fifo");
        exit(1);
    }
    printf("FIFO ouvert (id=%d)\n", idFifo);
    printf("PID de mon fils : %d\n", sig1fifo);
    // PID vers String
    char str[12];
    sprintf(str, "%d", sig1fifo);
    write(idFifo, str, strlen(str)+1);
    printf("Ecriture\n");
    close(idFifo);

    // Suppression du FIFO
    unlink(FIFO_NAME);
    return 0;
}
