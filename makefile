all: main

main: main.o
	gcc -o main main.o

main.o: main.c
	gcc -o main.o -c main.c -W -Wall -ansi -pedantic

ex2: ex2.o
	gcc -o ex2 ex2.o

ex2.o: ex2.c
	gcc -o ex2.o -c ex2.c -W -Wall -ansi -pedantic

clean:
	rm -rf *.o
