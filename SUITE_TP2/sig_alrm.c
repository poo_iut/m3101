#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <setjmp.h>

void traiterSignal(int numSignal);

typedef void(* PtrFct) (int);

static jmp_buf buf;

int main(int argc, char const *argv[]) {

    pid_t pid = getpid();

    printf("PID : %d\n", pid);

    PtrFct res = signal(SIGALRM, traiterSignal);

    if(SIG_ERR == res){
        printf("Erreur définition signal\n");
        exit(-1);
    }

    // Saisie au clavier
    char* line = NULL;
    size_t taille = 0;
    // Set jump
    int nbSaisie = 0;
    int jumpValue = setjmp(buf);
    if(jumpValue = 1){
        if(nbSaisie >= 3){
            exit(1);
        }
    }
    nbSaisie++;
    printf("Saisie restante : %d\n", (4-nbSaisie));
    // Alarme dans 3 secondes
    int alarm_ = alarm(3);
    printf("Saisir un truc\n");
    getline(&line, &taille, stdin);
    return 0;
}

void traiterSignal(int numSignal){
    if(numSignal == SIGALRM){
        sigrelse(numSignal);
        longjmp(buf, 1);
    }
}
