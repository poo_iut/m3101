#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>

int main(int argc, char const *argv[]) {
    if(argc != 3){
        printf("Veuillez renseigner un PID et un signal.\n");
        exit(1);
    }
    int pid = atoi(argv[1]);
    int sig = atoi(argv[2]);
    printf("Signal : %d, PID : %d\n", sig, pid);
    kill(pid, sig);
}
