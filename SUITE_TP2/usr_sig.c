
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>

void traiterSignal(int numSignal);

typedef void(* PtrFct) (int);

int main(int argc, char const *argv[]) {

    pid_t pid = getpid();

    printf("PID : %d\n", pid);

    PtrFct res = signal(SIGUSR1, traiterSignal);
    PtrFct res2 = signal(SIGUSR2, traiterSignal);

    if(SIG_ERR == res || SIG_ERR == res2){
        printf("Erreur signal\n");
        exit(-1);
    }
    while(1);
    return 0;
}

void traiterSignal(int numSignal){
    printf("Numéro signal reçu: %d\n", numSignal);
    // A FAIRE SURTOUT POUR GAGNER DES POINTS MMMMMHHH LES BONS POINTS
    if(numSignal == SIGUSR1){
        exit(0);
    }else{
        printf("Mauvais signal reçu\n");
        exit(-2);
    }
}
